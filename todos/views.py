# from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy, reverse
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "tododetail"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    context_object_name = "todocreate"
    fields = ["name"]
    success_url = reverse_lazy("todos_list_list")


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    context_object_name = "todoupdate"
    fields = ["name"]

    def get_success_url(self):
        return reverse("todos_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items_create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list_detail")

    def get_success_url(self):
        return reverse("todos_list_detail", args=[self.object.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items_update.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list_detail")
    context_object_name = "todoitemupdate"

    def get_success_url(self):
        return reverse("todos_list_detail", args=[self.object.id])
